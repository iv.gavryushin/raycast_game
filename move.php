<?php

//Matrix for rotation
$direction_x = array(0,10,10,10,0, -10, -10, -10);
$direction_y = array(10,10,0, -10, -10, -10, 0,10);

//Check for vars  
if (!isset($a) && !isset($b))
{
    $a = 0;
    $b = 0;
}

if (!isset($x) && !isset($y))
{
    $x = 0;
    $y = 10;
    $i = - 1;
}

//Rotation here
$i = $_GET['i'];

if (isset($_GET['rotate_clockwise']))
{
    if ($i == 7) $i = - 1;
    $i++;
    $x = $direction_x[$i];
    $y = $direction_y[$i];
}

if (isset($_GET['rotate_counter_clockwise']))
{
    if ($i == 0) $i = 8;
    $i--;
    $x = $direction_x[$i];
    $y = $direction_y[$i];
}

if (isset($_GET['forward']))
{
    if ($_GET['i'] == '') $i = 0;
    $x = $_GET['x'];
    $y = $_GET['y'];

    $a = $_GET['a'] + ($direction_x[$i]);
    $b = $_GET['b'] + ($direction_y[$i]);

}
else
{
    $a = $_GET['a'];
    $b = $_GET['b'];

}
?>
<style>
			#point_of_view {
			position: absolute;
			top:  <?=$x + 150 + $a ?>px;
			left: <?=$y + 150 + $b ?>px;
			background-color: green;
			width: 5px;
			height: 5px;
			}	
		
			#point_zero {
			position: absolute;
			top:  <?=$a + 150 ?>px;
			left: <?=$b + 150 ?>px;
			background-color: #333;
			width: 5px;
			height: 5px;
			}
			
</style>


<div id="point_of_view">
</div>
<div id="point_zero">
</div>


<form  method="GET" align="right">
<input type="submit" name="rotate_clockwise"  value="____rotate_clockwise_____">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
</form>

<form  method="GET" align="right">
<input type="submit" name="rotate_counter_clockwise"  value="_rotate_counter_clockwise_">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
</form>


<form  method="GET" align="right">
<input type="submit" name="forward" value="move">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
</form>
