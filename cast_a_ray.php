<?php


//Часть II - Бросание лучей
//FOV - определяет угол зрения (поле зрения).  
$fov=0;
//POV - point of view
$pov=0.523599; //30 degree
//Рейкастинг - бросание лучей. j - хранит  количество лучей - это определяет поле зрения (FOV).
	for ($j=0; $j != 11; $j++) {
$ray_angle=$pov+$fov;

//Вычисляем угол следующего луча. Отнимаем 0.05 градуса. FOV - field of view - поле обзора.
//Используем матрицу поворота
$x1= floor($x*cos($pov+$fov)-$y*sin($pov+$fov));
$y1= floor($x*sin($pov+$fov)+$y*cos($pov+$fov));
//Новый луч под углом -5 градусов от прежнего
$fov-=0.087266;

//Векторы
$vecx=$x1;
$vecy=$y1;


//Бросаем луч. k - влияет на длину луча
	for ($k=1; $k != 30; $k++) {		
	$vecx+=$x1;
	$vecy+=$y1;

	//Рисуем лучи синим цветом
print '<div id="light_of_rays" style="height: 1px; top: '.($vecx+150+$a).'px; left: '.($vecy+150+$b). 'px;"></div>';
			
			//Отдача. Собираем в массив
			//Ищем совпадения координат стенок с лучом (плотность лучей низкая, возможны пропуски)
			//Indirect rendering
			foreach ($mapx as $kv => $v) {
			if ( ($vecx+150+$a)==$v && $mapy[$kv] == ($vecy+150+$b)) {
			///Длина луча - теорема Пифагора :)
			//$render[]=sqrt((($vecx+150+$a)*($vecx+150+$a))+(($vecy+150+$b)*($vecy+150+$b)));
			//Можно использовать количество циклов для определения длины луча k 
			//Устраним эффект рыбьего глаза
			$render[]=$k*cos(atan2($x,$y)-$ray_angle);
			break 2;
			}
		}	
	}
	//Запишем 100, если луч не встретил стену (при отдаче колонка (100) минус 100 даст 0 - видимые
	//проходы между стенами )
	if ($k==30) $render[]=100;
	
}
		
?>
<style>
			#light_of_rays {
			position: absolute;
			background-color: blue;
			width: 3px;
			height: 3px;
			}
			#wall_real {
			position: absolute;
			bottom:  315px;
			left: 30px;
			background-color: #666;
			width:  25px;
			height: 25px;
			}		
					
</style>
